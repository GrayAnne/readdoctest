This project is a test of using docker

All process is tested in Ubuntu 20.04

source: `Install Docker Desktop on Linux \| Docker
Documentation <https://docs.docker.com/desktop/install/linux-install/>`__

Docker tutorial
===============

How to install docker
~~~~~~~~~~~~~~~~~~~~~


Uninstall old versions
----------------------

::

   for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done

Set up the repository
---------------------

1. Update the ``apt`` package index and install packages to allow
   ``apt`` to use a repository over HTTPS:

   ::

      sudo apt-get update
      sudo apt-get install ca-certificates curl gnupg

2. Add Docker’s official GPG key:

   ::

      sudo install -m 0755 -d /etc/apt/keyrings
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
      sudo chmod a+r /etc/apt/keyrings/docker.gpg

3. Use the following command to set up the repository:

   ::

      echo \
        "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
        "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

Install Docker Engine
---------------------

1. Update the apt package index:

::

       sudo apt-get update

2. Install Docker Engine, containerd, and Docker Compose. To install the
   latest version, run:

::

       sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

3. Verify that the Docker Engine installation is successful by running
   the hello-world image.

::

       sudo docker run hello-world

How to pull images
~~~~~~~~~~~~~~~~~~

Pull images from gitlab
-----------------------

1. Use the ``docker login`` command to authenticate with the GitLab
   container registry. Run the following command and provide your GitLab
   username and password

::

   (sudo) docker login registry.gitlab.com

2. pull the Docker image using the ``docker pull`` command followed by
   the image URL

::

   (sudo) docker pull registry.gitlab.com/<namespace>/<project>/<image>:<tag>

for example

::

   (sudo) docker pull registry.gitlab.com/op-cps/test-docker:latest

How to use
~~~~~~~~~~

Use the command ``run`` to launch the docker container

::

   xhost +
   docker run [OPTIONS] IMAGE [COMMAND]  [ARG...]

for example

::

   xhost +
   (sudo) docker run --net host -v /tmp/.X11-unix:/tmp/.X11-unix -it registry.gitlab.com/op-cps/test-docker:latest

``xhost +`` and ``--net host -v /tmp/.X11-unix:/tmp/.X11-unix`` can
enable GUI applications such as rviz and rqt on Docker Containers.

Run a new bash shell in a running container
-------------------------------------------

::

   docker exec -it "id of running container" bash

End Docker
----------

Input ``exit`` to quit docker

Use command ``prune`` to clean stopped containers

::

   (sudo) docker container prune
