
test scripts
============

Download scripts
~~~~~~~~~~~~~~~~
::

   git clone #todo

launch_scripts
~~~~~~~~~~~~~~
Unitree
-------
open 3 terminal

1. In the 1st terminal: launch unitree gazebo simulator
::

      source unitree_launch_world.sh
      
2. In the 2nd terminal: start keyboard control
::

      source unitree_keyboard_control.sh

First press 2 to standup, then press 4 for trotting, keys w,a,s,d and j,l for moving and rotating

3. In the 3rd terminal: launch the script to assign directions for unitree dog automatically
::

      source unitree_launch_world.sh
      
.. note::
      unitree_keyboard_simulation.sh simulates keyboard operations, so need to return to the 2nd terminal after launched the script

Jackal
------
open 4 terminal

1. In the 1st terminal: launch jackal gazebo simulator
::

      # launch jackal_world
      source jackal_launch_race.sh
      
      # or launch office_world
      source jackal_launch_office.sh
      
2. In the 2nd terminal: launch the Cartographer node to begin SLAM
::

      source jackal_launch_nav.sh


3. In the 3rd terminal: launch RViz to visualize the robot
::

      source jackal_launch_rviz.sh

4. In the 4th terminal: assign waypoints to jackal automatically
::

      # for jackal_world
      source jackal_auto_race.sh
      
      # for office_world
      source jackal_auto_office.sh

TurtleBot3
------
open 4 terminal

1. In the 1st terminal: launch turtleBot3 gazebo simulator
::

      # launch turtleBot3_world
      source turtleBot3_launch_world.sh
      
      # or launch turtleBot3_house
      source turtleBot3_launch_office.sh
      
2. In the 2nd terminal: run navigation node
::

      # for turtleBot3_world
      source turtleBot3_launch_nav_world.sh 
      
      # for turtleBot3_house
      source turtleBot3_launch_nav_office.sh

3. In the 3rd terminal: launch RViz to visualize the robot
::

      source turtleBot3_launch_rviz.sh

4. In the 4th terminal: assign waypoints to turtleBot3 automatically
::

      # for turtleBot3_world
      source turtleBot3_auto_world.sh
      
      # for turtleBot3_house
      source turtleBot3_auto_house.sh

Px4
-------
open 2 terminal

1. In the 1st terminal: launch px4 gazebo simulator
::

      source px4_launch_gazebo.sh
      
2. In the 2nd terminal: launch QGroundControl for px4
::

      source px4_launch_QG.sh  

Op3
-------

todo

