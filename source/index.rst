.. OP-CPS documentation master file, created by
   sphinx-quickstart on Tue Jul 11 11:36:29 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

OP-CPS
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   docker
   test_scripts

 

indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
